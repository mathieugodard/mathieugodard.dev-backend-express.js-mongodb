const mongoose = require("mongoose");

const postSchema = mongoose.Schema({
    title: { type: String, required: true, trim: true },
    chapo: { type: String, required: true, trim: true },
    content: { type: String, required: true },
    status: { type: String, required: true, trim: true },
    featuredImage: { type: String, required: false, trim: true },
    slug: { type: String, required: true, trim: true },
    languages: {
        type: String,
        enum: {
            values: ["N/A", "HTML", "CSS", "JavaScript", "Markdown", "Python"],
            message: 'la valeur "{VALUE}" n\'est pas supportée',
        },
        required: true,
    },
    categories: {
        type: String,
        enum: {
            values: [
                "Non classé",
                "Backend",
                "Frontend",
                "Framework",
                "Librairies",
                "Angular",
                "Node.js",
                "Express.js",
                "MongoDB",
                "PostgreSQL",
                "Accessibilité",
                "Eco-conception",
                "Juridique",
                "Protection des données",
                "Protocoles Web",
                "Performance",
                "Sécurité",
                "Logiciels",
                "Vue.js",
                "Nuxt.js",
                "Django",
                "WordPress",
            ],
            message: 'la valeur "{VALUE}" n\'est pas supportée',
        },
        required: true,
    },
    userId: { type: String, required: true },
    dateCreate: { type: Date, required: true, default: Date.now },
    dateUpdate: { type: Date, required: true, default: Date.now },
});

module.exports = mongoose.model("Post", postSchema);
