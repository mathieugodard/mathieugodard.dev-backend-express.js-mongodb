const Post = require("../models/postModel");

exports.createPost = (req, res, next) => {
    // const postObject = req.body;
    // console.log(postObject);
    const post = new Post({
    // title: "Un titrum",
    // chapo: "Lorem",
    // content: "lorem ipsum",
    // status: "published",
    // featuredImage: "une-image.jpg",
    // slug: "Un super slug",
    // languages: "Machin",
    // categories: "Truc",
    // userId: "Pouet",
    // dateCreate: Date.now,
    // dateUpdate: Date.now,
    });
    console.log(post);
    post.save()
        .then(() => {
            res.status(201).json({ message: "Post created" });
        })
        .catch((error) => {
            res.status(400).json({ error });
        });
};

exports.modifyPost = (req, res, next) => {};

exports.deletePost = (req, res, next) => {};

exports.getOnePost = (req, res, next) => {};

exports.getAllPosts = (req, res, next) => {};
